import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import Errors from './common/errors'

Vue.config.productionTip = false

Vue.use(BootstrapVue)

Vue.config.errorHandler = function(err, vm, info) {
  console.error(`Unhandled Error: ${err.toString()}\nInfo: ${info}`);

  const wrappedError = Errors.ErrorWrapper.wrapError(err)
  vm.$bvToast.toast(wrappedError.message, {
    title: `Ошибка #${wrappedError.code}`,
    autoHideDelay: 5000,
    appendToast: true,
    variant: "danger"
  })
}

Vue.prototype.$eventHub = new Vue(); // Global event bus

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
