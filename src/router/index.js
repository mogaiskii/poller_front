import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '../views/Auth.vue'
import Home from '../views/Home.vue'
import PollUser from '../views/PollUser.vue'

import AuthModule from '../core/auth'

Vue.use(VueRouter)


const onlyAuthentecated = (to, from, next) => {
  return AuthModule.isAuthenticated()
    .then(
      value => {
        if (value === true) {
          next()
        } else {
          next('/')
        }
      }
    )
    .catch(() => {
      next('/')
    })
}
console.log(onlyAuthentecated.toString()[0])

const excludeAuthentecated = (to, from, next) => {
  return AuthModule.isAuthenticated()
    .then(
      value => {
        if (value === true) {
          next('/home')
        } else {
          next()
        }
      }
    )
    .catch(() => {
      next()
    })
}

const routes = [
  {
    path: '/',
    name: 'Auth',
    component: Auth,
    beforeEnter: excludeAuthentecated
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    beforeEnter: onlyAuthentecated
  },
  {
    path: '/poll/:id',
    name: 'PollUser',
    component: PollUser,
    beforeEnter: onlyAuthentecated
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
