import Codes from './error_codes'

class ErrorWrapper{
    static DEFAULT_ERROR_CODE = 3
    static lastId = 0

    static wrapError(error) {
        let code = ErrorWrapper.DEFAULT_ERROR_CODE
        if (Object.prototype.hasOwnProperty.call(error, 'code')){
            code = error.code
        }
        const message = Codes[code]
        return {
            id: ErrorWrapper.lastId++,
            code,
            message
        }
    }
}

class ConnectionError extends Error {
    static code = 1
    constructor(message) {
      super(message);
      this.name = "ConnectionError";
      this.code = ConnectionError.code
    }
}

class ServerError extends Error {
    static code = 2
    constructor(message) {
      super(message);
      this.name = "ServerError";
      this.code = ServerError.code
    }
}

class ApplicationError extends Error {
    static code = 3
    constructor(message) {
      super(message);
      this.name = "ApplicationError";
      this.code = ApplicationError.code
    }
}

class AuthError extends Error {
    static code = 4
    constructor(message) {
      super(message);
      this.name = "AuthError";
      this.code = AuthError.code
    }
}

class ForbiddenError extends Error {
    static code = 5
    constructor(message) {
        super(message);
        this.name = ForbiddenError
        this.code = ForbiddenError.code
    }
}

class NotFoundError extends Error {
    static code = 6
    constructor(message) {
        super(message);
        this.name = NotFoundError
        this.code = NotFoundError.code
    }
}

export default {
    ErrorWrapper,
    ConnectionError,
    ServerError,
    ApplicationError,
    AuthError,
    ForbiddenError,
    NotFoundError
}
