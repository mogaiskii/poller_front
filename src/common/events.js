export default {
    loading: 'loading',
    stopLoading: 'stopLoading',
    error: 'error'
}