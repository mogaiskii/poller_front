import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const POLL_POSTFIX = '/boss_route'


function makeUri(route) {
    return Config.host + POLL_POSTFIX + route
}

async function bossPollsCardAdminPost(token, body) {
    const ROUTE = '/polls'

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossPollsCardAdminGet(token, id) {
    const ROUTE = '/polls/' + id

    const response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossPollsUpdateCardAdminPost(token, id, body) {
    const ROUTE = '/polls/' + id

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}


export default {
    bossPollsCardAdminPost,
    bossPollsCardAdminGet,
    bossPollsUpdateCardAdminPost,
}
