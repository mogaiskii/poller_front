import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const POLL_POSTFIX = '/polls'


function makeUri(route) {
    return Config.host + POLL_POSTFIX + route
}


async function pollsList(token, limit=null, offset=null) {
    const ROUTE = '/'

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendGetRequest(makeUri(ROUTE), token, query, null)

    if (response.status == 404) throw new CommonErrors.ServerError('Polls list not found')

    return response.data
}

async function pollCard(token, id) {
    const ROUTE = '/' + id

    const response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)

    if (response.status == 404) throw new CommonErrors.ApplicationError('Poll not found')

    return response.data
}

async function pollVote(token, id, poll_structure) {
    const ROUTE = '/' + id

    const body = { poll_structure }

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 403) throw new CommonErrors.ForbiddenError('User not allowed')

    if (response.status == 404) throw new CommonErrors.ApplicationError('Poll not found')

    return response.data
}


export default {
    pollsList,
    pollCard,
    pollVote
}
