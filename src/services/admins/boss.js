import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const POLL_POSTFIX = '/boss_route'


function makeUri(route) {
    return Config.host + POLL_POSTFIX + route
}

async function bossPollsCreateCardAdminPost(token, body) {
    const ROUTE = '/polls'

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossPollsGetCardAdminGet(token, id) {
    const ROUTE = '/polls/' + id

    const response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossPollsUpdateCardAdminPost(token, id, body) {
    const ROUTE = '/polls/' + id

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossGetUsersListGet(token, limit=null, offset=null) {
    const ROUTE = '/users'

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendGetRequest(makeUri(ROUTE), token, query, null)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossCreateUserPost(token, limit=null, offset=null, body) {
    const ROUTE = '/users'

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendPostRequest(makeUri(ROUTE), token, query, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossUsersSearchPost(token, limit=null, offset=null, body) {
    const ROUTE = '/users/search'

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendPostRequest(makeUri(ROUTE), token, query, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossGetUserGet(token, limit=null, offset=null, id) {
    const ROUTE = '/users/' + id

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendGetRequest(makeUri(ROUTE), token, query, null)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossUpdateUserPost(token, limit=null, offset=null, id, body) {
    const ROUTE = '/users/' + id

    let query = {}
    if (limit != null) query.limit = limit
    if (offset != null) query.offset = offset

    const response = await api.sendPostRequest(makeUri(ROUTE), token, query, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}
async function bossSetInfoPost(token, body) {
    const ROUTE = '/info'

    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}

export default {
    bossPollsCreateCardAdminPost,
    bossPollsGetCardAdminGet,
    bossPollsUpdateCardAdminPost,
    bossGetUsersListGet,
    bossCreateUserPost,
    bossUsersSearchPost,
    bossGetUserGet,
    bossUpdateUserPost,
    bossSetInfoPost,
}
