import Config from '../../config'
import CommonErrors from '../../common/errors'
import ServiceErrors from './errors'
import api from '../api'

const AUTH_POSTFIX = '/auth'


function makeUri(route) {
    return Config.host + AUTH_POSTFIX + route
}


/**
 *
 *
 * @param {*} code string - auth code from user
 * @returns string - auth token
 */
async function auth(code) {
    const ROUTE = '/'

    let response = await api.sendPostRequest(makeUri(ROUTE), null, null, {code})

    if (response.status == 403) throw new ServiceErrors.AuthError('Wrong token')
    if (response.status == 404) throw new CommonErrors.ServerError('Server Error')

    if (!Object.prototype.hasOwnProperty.call(response.data, 'auth_token')) throw new CommonErrors.ApplicationError('a')

    return response.data.auth_token
}

/**
 *
 *
 * @param {*} token string - auth token
 * @returns bool - is user authorized
 */
async function check(token) {
    const ROUTE = '/'

    let response = {}

    try {
        response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)
    } catch (error) {
        if (Object.prototype.hasOwnProperty.call(error, 'code')) {
            if (error.code == 4) {
                return false
            }
        }
        throw error
    }

    if (response.status == 404) throw new CommonErrors.ServerError('Server Error')

    if (response.status == 200) return true

    return false
}

export default {
    auth,
    check
}
