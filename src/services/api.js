import CommonErrors from '../common/errors'


/**
 * used for converting js object to query string (php-format arrays)
 *
 * @param {*} obj
 * @param {*} prefix
 * @returns
 */
function querySerializer(obj, prefix) {
    var str = [],
      p;
    for (p in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
        str.push((v !== null && typeof v === "object") ?
          querySerializer(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}


/**
 * used for sending POST request
 *
 * @param {*} uri string (without query)
 * @param {*} [token=null] string (Authorization)
 * @param {*} [query=null] object (query params)
 * @param {*} [jsonBody=null] object (body content)
 * @returns
 */
async function sendPostRequest(uri, token=null, query=null, jsonBody=null) {
    const requestData = {method: 'POST'}
    let requestUri = uri;

    requestData.headers = {'Content-Type': 'application/json'}

    if (token != null) {
        requestData.headers['Authorization'] = token
    }
    if (query != null) {
        requestUri += querySerializer(query)
    }
    if (jsonBody != null) {
        requestData.body = JSON.stringify(jsonBody)
    }

    let response
    try{
        response = await fetch(requestUri, requestData)
    } catch (error) {
        throw new CommonErrors.ConnectionError('Error on requesting server')
    }

    if (response.status == 500) throw new CommonErrors.ServerError('Server Error')
    if (response.status == 401) throw new CommonErrors.AuthError('')

    response.data = await response.json()

    return response
}


/**
 * used for sending GET request
 *
 * @param {*} uri string (without query)
 * @param {*} [token=null] string (Authorization)
 * @param {*} [query=null] object (query params)
 * @returns
 */
async function sendGetRequest(uri, token=null, query=null) {
    const requestData = {method: 'GET'}
    let requestUri = uri;

    requestData.headers = {'Content-Type': 'application/json'}

    if (token != null) {
        requestData.headers['Authorization'] = token
    }

    if (query != null) {
        requestUri += querySerializer(query)
    }

    let response
    try{
        response = await fetch(requestUri, requestData)
    } catch (error) {
        throw new CommonErrors.ConnectionError('Error on requesting server')
    }

    if (response.status == 500) throw new CommonErrors.ServerError('Server Error')
    if (response.status == 401) throw new CommonErrors.AuthError('')

    response.data = await response.json()

    return response
}

export default {
    sendGetRequest,
    sendPostRequest
}