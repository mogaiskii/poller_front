import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const POLL_POSTFIX = '/superfeatures'


function makeUri(route) {
    return Config.host + POLL_POSTFIX + route
}


async function IAmBadGet (token) {
    const ROUTE = '/am_i_bad/'
    const response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}

export default {
    IAmBadGet,
}
