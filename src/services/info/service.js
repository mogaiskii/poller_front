import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const INFO_POSTFIX = '/info'


function makeUri(route) {
    return Config.host + INFO_POSTFIX + route
}

async function getInfo(token) {
    const ROUTE = '/'
    const response = await api.sendGetRequest(makeUri(ROUTE), token, null, null)

    if (response.status == 404) throw new CommonErrors.ServerError('Info not found')

    return response.data
}


export default {
    getInfo,
}
