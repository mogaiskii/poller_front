import ServiceSubmodule from "./service"
import BossSubmodule from "./boss"

export default {
    ...ServiceSubmodule,
    ...BossSubmodule
}
