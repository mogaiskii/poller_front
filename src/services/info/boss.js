import Config from '../../config'
import CommonErrors from '../../common/errors'
import api from '../api'

const POLL_POSTFIX = '/boss_routes'


function makeUri(route) {
    return Config.host + POLL_POSTFIX + route
}

async function setInfoPost(token, body) {
    const ROUTE = '/info'
    const response = await api.sendPostRequest(makeUri(ROUTE), token, null, body)

    if (response.status == 404) throw new CommonErrors.ServerError('a')

    return response.data
}

export default {
    setInfoPost,
}
