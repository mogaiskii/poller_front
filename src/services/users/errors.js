
class AuthError extends Error {
    static code = 100
    constructor(message) {
      super(message); // (1)
      this.name = "AuthError"; // (2)
      this.code = AuthError.code
    }
}


export default {
    AuthError
}
