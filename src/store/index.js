// NOTE: just simple repository
// TODO: add data models

import Vue from 'vue'
import Vuex from 'vuex'
// import {ErrorWrapper, ApplicationError} from '../common/errors'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    polls: [],
    poll: null,
    info: null,
    users: [],
    user: null,
  },
  mutations: {
    SET_POLLS(state, polls) {
      state.polls = polls
    },
    SET_POLL(state, poll){
      state.poll = poll
    },
    SET_INFO(state, info){
      state.info = info
    },
    SET_USERS(state, users){
      state.users = users
    },
    SET_USER(state, user){
      state.user = user
    }
  },
  getters: {
    getPolls: state => state.polls,
    getPoll: state => state.poll,
    getInfo: state => state.info,
    getUsers: state => state.users,
    getUser: state => state.user
  },
  actions: {
  },
  modules: {
  }
})
