import Cookies from 'js-cookie'
import AuthService from '@/services/auth'

const AUTH_TOKEN_COOCKIE = '_poll_auth_token'

async function hasToken() {
    if (Cookies.get(AUTH_TOKEN_COOCKIE)) return true
    return false
}

async function getToken() {
    if (!hasToken) return undefined
    return Cookies.get(AUTH_TOKEN_COOCKIE)
}

async function isAuthenticated() {
    const hasTokenResult = await hasToken()
    if (!hasTokenResult) return false
    const token = await getToken()

    return await AuthService.default.check(token)

}

async function saveAuthentication(token) {
    Cookies.set(AUTH_TOKEN_COOCKIE, token)
}

async function clearAuthentication() {
    Cookies.remove(AUTH_TOKEN_COOCKIE)
}

async function updateAuthentication(token) {
    return await saveAuthentication(token)
}

export default {
    hasToken, getToken, isAuthenticated, saveAuthentication, clearAuthentication, updateAuthentication

}
