import SuperfeaturesService from '@/services/superfeatures'
// TODO: fix it. need upper-level abstraction to communicate modules
import AuthModule from '@/core/auth'
// import store from '@/store'

let interval = null


function startPolling() {
    if (interval != null) return
    interval = setInterval(function() {
        AuthModule.getToken()
            .then(token => {
                return SuperfeaturesService.default.IAmBadGet(token)
            })
            .then(response => {
                console.log(response)
            })
    }, 10000)
}

function stopPolling() {
    if (interval == null) return
    clearInterval(interval)
    interval = null
}


export default {
    startPolling,
    stopPolling
}
