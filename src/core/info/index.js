import InfoService from '@/services/info'
// TODO: fix it. need upper-level abstraction to communicate modules
import AuthModule from '@/core/auth'
import store from '@/store'


async function getInfo() {
    let info = store.getters.getInfo

    if (info == null) {
        const token = await AuthModule.getToken()
        info = await InfoService.getInfo(token)
        store.commit("SET_INFO", info)
    }

    return info
}


export default {
    getInfo
}
