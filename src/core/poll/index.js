import Vue from 'vue'
import store from '@/store'
import PollService from '@/services/poll'
// TODO: fix it. need upper-level abstraction to communicate modules
import AuthModule from '@/core/auth'


function voteVariantUp(variant){
    const poll = store.getters.getPoll

    for (const row of poll.poll_structure) {
        // TODO: hardcode
        if (row.type == 'variant') {
            for (const rowVariant of row.variants) {
                if (rowVariant.id == variant.id) {
                    const rowCounter = getRowCounter(row)
                    if (rowCounter != null && rowCounter <= 0) return

                    rowVariant.voted_by_user += 1
                }
            }
        }
    }

    // TODO: hardcode
    store.commit('SET_POLL', poll)
}


function voteVariantDown(variant){
    const poll = store.getters.getPoll

    for (const row of poll.poll_structure) {
        // TODO: hardcode
        if (row.type == 'variant') {
            for (const rowVariant of row.variants) {
                if (rowVariant.id == variant.id && rowVariant.voted_by_user > 0) {

                    rowVariant.voted_by_user -= 1
                }
            }
        }
    }

    // TODO: hardcode
    store.commit('SET_POLL', poll)
}

function getVariantCounter(variant){
    return variant.voted_by_user
}

function getVariantRow(variant) {
    const poll = store.getters.getPoll

    for (const row of poll.poll_structure) {
        // TODO: hardcode
        if (row.type == 'variant') {
            for (const rowVariant of row.variants) {
                if (rowVariant.id == variant.id) {
                    return row
                }
            }
        }
    }
    return null
}

function getRangeRow(range){
    const poll = store.getters.getPoll

    for (const row of poll.poll_structure) {
        // TODO: hardcode
        if (row.type == 'range') {
            if (row.range.id == range.id) {
                return row
            }
        }
    }

    return null
}

function getRowCounter(pollRow){
    // TODO: hardcode
    if (pollRow.type != 'variant') {
        return null
    }

    if (!Object.prototype.hasOwnProperty.call(pollRow, 'row_available_votes_actions')) {
        return null
    }

    let counter = pollRow.row_available_votes_actions
    for (const variant of pollRow.variants) {
        counter -= variant.voted_by_user
    }

    const pollCounter = getPollCounter()
    if (pollCounter < counter) return pollCounter

    return counter
}

// TODO: move to getters, so it would caches
function getPollCounter(){
    const poll = store.getters.getPoll

    if (!Object.prototype.hasOwnProperty.call(poll, 'available_votes_actions')) {
        return null
    }

    let counter = poll.available_votes_actions

    if (counter == null || counter == 0) return null

    for (const row of poll.poll_structure) {
        if (row.type == 'variant') {
            for (const variant of row.variants) {
                counter -= variant.voted_by_user
            }
        }
    }
    return counter
}

// NOTE: mutations with fake

/**
 * validates current poll form
 *
 * @returns boolean - is poll valid
 */
function validatePoll() {
    const poll = store.getters.getPoll

    delete poll.error

    let hasError = false

    const hasActions = getPollCounter()

    if (hasActions) hasError = true
    
    for (const row of poll.poll_structure) {
        delete row.error
        if (row.type == 'range') {
            if (row.range.value == null) {
                Vue.set(row, 'error', 'выберите значение')
                hasError = true
            }
        }
        if (row.type == 'variant') {
            console.log(row.row_available_votes_actions)
            if (row.row_available_votes_actions && hasActions) {
                Vue.set(row, 'error', 'выберите варианты')
                // hasError = true
            }
        }
    }

    if (hasError) Vue.set(poll, 'error', 'Заполните все поля')

    store.commit('SET_POLL', poll)

    return !hasError
}


function updateRange(range, value) {
    const poll = store.getters.getPoll

    for (const row of poll.poll_structure) {
        // TODO: hardcode
        if (row.type == 'range') {
            if (row.range.id == range.id) {
                row.range.value = value
                row.range.voted = true
            }
        }
    }

    // TODO: hardcode
    store.commit('SET_POLL', poll)
}


async function sendPollVotes() {
    const poll = store.getters.getPoll

    const token = await AuthModule.getToken()

    const response = await PollService.pollVote(token, poll.id, poll.poll_structure)
    store.commit('SET_POLL', response)
    return response
}


async function getPollList() {
    const token = await AuthModule.getToken()

    const response = await PollService.pollsList(token)
    const polls = response.polls
    store.commit('SET_POLLS', polls)
    return polls
}

async function getPollCard(id) {
    const token = await AuthModule.getToken()

    // TODO: do smth on 404
    const response = await PollService.pollCard(token, id)
    store.commit('SET_POLL', response)
    return response
}


export default {
    voteVariantUp,
    voteVariantDown,
    getVariantCounter,
    getRowCounter,
    getPollCounter,
    getVariantRow,
    getRangeRow,
    updateRange,
    sendPollVotes,
    getPollList,
    getPollCard,
    validatePoll
}
