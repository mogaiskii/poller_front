openapi: "3.0.0"
info:
  version: 0.0.1
  title: Poll api
  description: Poll api
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0.html
servers:
  - url: /api
  - url: http://localhost:5000
  - url: http://1.2.3.4/api
tags:
  - name: auth
    description: 'Auth module'
  - name: poll
    description: 'Poll module'
  - name: admins
    description: 'Admins module'
  - name: users
    description: 'Users module'
  - name: info
    description: 'Info module'
  - name: superfeatures
    description: 'Super features module'
paths:

  /auth/:
    get:
      description: Check token
      operationId: authCheck
      tags:
        - auth
      parameters: 
        - in: header
          name: Authorization
          schema:
            type: string
          required: true
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: wrong request data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      
    post:
      description: Perform auth
      operationId: AuthRequest
      tags:
        - auth
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestAuth'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseAuth'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'


  # polls

  /polls/:
    get:
      description: 'Get list of polls'
      operationId: PollsList
      tags:
        - poll
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10

      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollsList'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        

  /polls/{pollId}/:
    get:
      description: 'Get poll card'
      operationId: PollCard
      tags:
        - poll
      parameters: 
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: pollId
          schema:
            type: integer
          required: true
          description: 'Requested poll id'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollCard'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    
    post:
      description: 'Vote for poll'
      operationId: PollVote
      tags:
        - poll
      parameters: 
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: pollId
          schema:
            type: integer
          required: true
          description: 'Requested poll id'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestPollVote'
        
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollCard'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
                   

  # boss_route

  /boss_route/polls/:
    post:
      description: 'Create poll card for admin'
      operationId: PollCardAdminCreate
      tags:
        - poll
        - admins
      parameters: 
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestPollCardAdminCreateOrUpdate'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollCardAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /boss_route/polls/{pollId}:
    get:
      description: 'Get poll card for admin'
      operationId: PollCardAdmin
      tags:
        - poll
        - admins
      parameters: 
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: pollId
          schema:
            type: integer
          required: true
          description: 'Requested poll id'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollCardAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    post:
      description: 'Update poll card for admin'
      operationId: PollCardAdminUpdate
      tags:
        - poll
        - admins
      parameters: 
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: pollId
          schema:
            type: integer
          required: true
          description: 'Requested poll id'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestPollCardAdminCreateOrUpdate'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponsePollCardAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /boss_route/users/:
    get:
      description: 'Get list of users'
      operationId: AdminUsersList
      tags:
        - admins
        - users
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10

      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseUsersListAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      description: 'Create user'
      operationId: AdminUsersCreate
      tags:
        - admins
        - users
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestUserCreateOrUpdateAdmin'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseUserAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  
  /boss_route/users/search/:
    post:
      description: 'Search users'
      operationId: AdminUsersSearch
      tags:
        - admins
        - users
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestUserSearchAdmin'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseUsersListAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /boss_route/users/{userId}:
    get:
      description: 'Get user'
      operationId: AdminUsersCard
      tags:
        - admins
        - users
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: userId
          schema:
            type: integer
          required: true
          description: 'Requested user id'
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseUserAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      description: 'Update user'
      operationId: AdminUsersUpdate
      tags:
        - admins
        - users
      parameters:
        - in: header
          name: Authorization
          required: true
          schema:
            type: string
        - in: path
          name: userId
          schema:
            type: integer
          required: true
          description: 'Requested user id'
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            minimum: 0
            example: 10
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestUserCreateOrUpdateAdmin'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseUserAdmin'
        '400':
          description: bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'


  # info
  
  /info/:
    get:
      description: Get info
      operationId: getInfo
      tags:
        - info
      parameters: 
        - in: header
          name: Authorization
          schema:
            type: string
          required: true
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseInfo'
        '400':
          description: wrong request data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /boss_routes/info/:
    post:
      description: Set info
      operationId: setInfoAdmin
      tags:
        - info
        - admins
      parameters: 
        - in: header
          name: Authorization
          schema:
            type: string
          required: true
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestInfoSetAdmin'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseInfo'
        '400':
          description: wrong request data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'


  # superfeatures

  /superfeatures/am_i_bad/:
    get:
      description: I am bad (or boss) superfeature
      operationId: amIBad
      tags:
        - superfeatures
      parameters: 
        - in: header
          name: Authorization
          schema:
            type: string
          required: true
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseAmIBad'
        '400':
          description: wrong request data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '403':
          description: not authorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '500':
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      
    


components:
  schemas:

    Success:
      required:
        - code
        - message
      properties:
        code:
          type: integer
          example: 200
        message:
          type: string
          example: 'OK'
    Error:
      required:
        - code
        - message
      properties:
        code:
          type: integer
          example: 500
        message:
          type: string
          example: 'Error message'

          
    RequestAuth:
      required:
        - code
      properties:
        code:
          type: string
          example: "1234"

    ResponseAuth:
      required:
        - auth_token
      properties:
        auth_token:
          type: string
          example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIxMjM0NTY3ODkwIiwiaWF0IjoxNTE2MjM5MDIyfQ.333KGrxZtgJaHZu5gtKSd4rmn0BYeVmEqcoelHXY508"
          
          
  # POLLS:
          
    ResponsePollBrief:
      required: 
        - id
        - poll_name
        - created_at
        - active
        - need_attention
      properties:
        id:
          type: integer
          example: 1
        poll_name:
          type: string
          example: "Go poll!"
        created_at:
          type: string
          format: date-time
          example: '2020-01-01T00:01:01Z'
        due_to:
          type: string
          description: 'Time of poll expiration'
          format: date-time
          example: '2020-01-01T00:01:01Z'
        voted_users:
          type: integer
          description: 'Count of users that already voted'
          example: 1
        active:
          type: boolean
          description: 'Is this poll available to vote'
          example: true
        available_votes_actions:
          type: integer
          description: 'How many `votes` does user has'
          example: 1
        need_attention:
          type: boolean
          description: 'signs that user should vote on this poll'
          example: false

    RequestPollCreateOrUpdate:
      required:
        - poll_name
        - created_at
        - active
      properties:
        id:
          type: integer
          example: 1
        poll_name:
          type: string
          example: "Go poll!"
        created_at:
          type: string
          format: date-time
          example: '2020-01-01T00:01:01Z'
        due_to:
          type: string
          description: 'Time of poll expiration'
          format: date-time
          example: '2020-01-01T00:01:01Z'
        active:
          type: boolean
          description: 'Is this poll available to vote'
          example: true
        available_votes_actions:
          type: integer
          description: 'How many `votes` does user has'
          example: 1
    

    ResponsePollVariant:
      required: 
        - id
        - title
        - voted_by_user
      properties:
        id:
          type: integer
          example: 1
        title:
          type: string
          example: "Variant 1"
        description:
          type: string
          example: "Variant 1 description"
        image_link:
          type: string
          example: "https://picsum.photos/600/800"
        voted_by_user:
          type: integer
          description: "How many times users voted"
          example: 1

    ResponsePollVariantVote:
      required:
        - id
        - voted_by_user
      properties:
        id:
          type: integer
          example: 1
        voted_by_user:
          type: integer
          description: 'How many votes user voted to this'
          example: 1
      

    RequestPollVariantCreateOrUpdate:
      required:
        - title
      properties:
        id:
          type: integer
          example: 1
        title:
          type: string
          example: "Variant 1"
        description:
          type: string
          example: "Variant 1 description"
        image_link:
          type: string
          example: "https://picsum.photos/600/800"


    ResponsePollVariantAdmin:
      allOf:
        - $ref: '#/components/schemas/ResponsePollVariant'
        - type: object
          required:
            - num_votes
            - voted_list
          properties:
            num_votes:
              type: integer
              description: "num of voted for this variant users"
              example: 1
            voted_list:
              type: array
              description: "ids of voted for this variant users"
              items:
                type: integer

    PollRange:
      required:
        - min_value
        - max_value
      properties:
        id:
          type: integer
          example: 1
        min_value:
          type: integer
          example: 1
        max_value:
          type: integer
          example: 100
        value:
          type: integer
          example: 50
        voted:
          description: 'was user already voted'
          type: boolean
          example: false

    PollRangeVoteAdmin:
      properties:
        id:
          type: integer
          example: 1
        uid:
          type: integer
          example: 1
        value:
          type: integer
          example: 50

    PollRangeAdmin:
      allOf:
        - $ref: '#/components/schemas/PollRange'
        - type: object
          properties:
            votes:
              type: array
              items:
                $ref: '#/components/schemas/PollRangeVoteAdmin'
          
    ResponsePollRangeVote:
      required:
        - id
        - value
      properties:
        id:
          type: integer
          example: 1
        value:
          type: integer
          example: 50

    ResponsePollRow:
      required:
        - id
        - row_name
      properties:
        id:
          type: integer
          example: 1
        row_name:
          type: string
          example: "Go poll!"
        row_available_votes_actions:
          type: integer
          description: 'How many `votes` does user has'
          example: 1
        type:
          type: string
          enum: ['variant', 'range']
        range:
         $ref: '#/components/schemas/PollRange'
        variants:
          type: array
          description: 'poll options (only if variant)'
          items:
                $ref: '#/components/schemas/ResponsePollVariant'
                
    RequestPollRowVote:
      required: 
        - id
      properties:
        id:
          type: integer
          example: 1
        range:
          $ref: '#/components/schemas/ResponsePollRangeVote'
        variants:
          type: array
          items:
            $ref: '#/components/schemas/ResponsePollVariantVote'

    RequestPollRowCreateOrUpdate:
      required:
        - row_name
      properties:
        id:
          type: integer
          example: 1
        row_name:
          type: string
          example: "Go poll!"
        row_available_votes_actions:
          type: integer
          description: 'How many `votes` does user has'
          example: 1
        type:
          type: string
          enum: ['variant', 'range']
        range:
         $ref: '#/components/schemas/PollRange'
        variants:
          type: array
          items:
                $ref: '#/components/schemas/RequestPollVariantCreateOrUpdate'

    ResponsePollRowAdmin:
      allOf:
        - $ref: '#/components/schemas/ResponsePollRow'
        - type: object
          properties:
            range:
             $ref: '#/components/schemas/PollRangeAdmin'
            variants:
              type: array
              items:
                    $ref: '#/components/schemas/ResponsePollVariantAdmin'

    ResponsePollsList:
      required: 
        - polls
      properties:
        polls:
          type: array
          items:
            $ref: '#/components/schemas/ResponsePollBrief'

    ResponsePollCard:
      allOf:
        - $ref: '#/components/schemas/ResponsePollBrief'
        - type: object
          required:
            - poll_structure
          properties:
            poll_structure:
              type: array
              items:
                $ref: '#/components/schemas/ResponsePollRow'
    
    RequestPollVote:
      required:
        - poll_structure
      properties:
        poll_structure:
          type: array
          items:
            $ref: '#/components/schemas/RequestPollRowVote'

    RequestPollCardAdminCreateOrUpdate:
      allOf:
        - $ref: '#/components/schemas/RequestPollCreateOrUpdate'
        - type: object
          required:
            - poll_structure
            - allowed_users
          properties:
            allowed_users:
              description: "ids of allowed to this vote users"
              type: array
              items:
                type: integer
            poll_structure:
              type: array
              items:
                $ref: '#/components/schemas/RequestPollRowCreateOrUpdate'

    ResponsePollCardAdmin:
      allOf:
        - $ref: '#/components/schemas/ResponsePollBrief'
        - type: object
          required:
            - poll_structure
            - allowed_users
          properties:
            allowed_users:
              description: "ids of allowed to this vote users"
              type: array
              items:
                type: integer
            poll_structure:
              type: array
              items:
                $ref: '#/components/schemas/ResponsePollRowAdmin'


  # USERS
    
    ResponseUserAdmin:
      required: 
        - id
        - name
        - code
        - is_staff
        - bad_guy
        - blocked
      properties:
        id:
          type: integer
          example: 1
        name:
          type: string
          example: "Sasha"
        code:
          type: string
          example: "1234"
        vk_id:
          type: string
          example: "id123456"
        is_staff:
          description: "is this user - admin"
          type: boolean
          example: false
        bad_guy:
          type: boolean
          description: "Is marked as bad guy (superfeature)"
          example: false
        blocked:
          type: boolean
          description: "Is banned"
          example: false
        phone:
          type: string
          example: "+79000000000"
        # super_on:
        #   type: array
        #   description: "id of votes(..orgs later) on which user is super"
        #   items:
        #     type: integer
        allowed_votes:
          type: array
          description: "id of votes(..orgs later) on which user is allowed to vote"
          items:
            type: integer
        friend_of:
          type: array
          description: "id of users that are friend of this user"
          items:
            type: integer

    RequestUserCreateOrUpdateAdmin:
      required: 
        - id
        - name
        - code
        - is_staff
        - bad_guy
        - blocked
      properties:
        id:
          type: integer
          example: 1
        name:
          type: string
          example: "Sasha"
        code:
          type: string
          example: "1234"
        vk_id:
          type: string
          example: "id123456"
        is_staff:
          description: "is this user - admin"
          type: boolean
          example: false
        bad_guy:
          description: "is this user - `bad guy`"
          type: boolean
          example: false
        blocked:
          description: "is this user baneed"
          type: boolean
          example: false
        phone:
          type: string
          example: "+79000000000"
        # super_on:
        #   type: array
        #   description: "id of votes(..orgs later) on which user is super"
        #   items:
        #     type: integer
        allowed_votes:
          type: array
          description: "id of votes(..orgs later) on which user is allowed to vote"
          items:
            type: integer
        friend_of:
          type: array
          description: "id of users that are friend of this user"
          items:
            type: integer

    ResponseUsersListAdmin:
      required:
        - users
      properties:
        users:
          type: array
          items:
            $ref: '#/components/schemas/ResponseUserAdmin'

    RequestUserSearchAdmin:
      properties:
        ids:
          type: array
          items:
            type: integer
        name:
          type: string
        phone:
          type: string
        vk_id:
          type: string


  # INFO
  
    ResponseInfo:
      required:
        - text
        - title
      properties:
        title:
          type: string
          description: 'browser title'
        text:
          type: string
          description: 'MarkDown text'

    RequestInfoSetAdmin:
      required:
        - text
        - title
      properties:
        title:
          type: string
          description: 'browser title'
        text:
          type: string
          description: 'MarkDown text'
      

  # SUPERFEATURES
  
    ResponseAmIBad:
      required:
        - i_am_bad
      properties:
        i_am_bad:
          type: boolean
          description: "is this user `bad guy`"
          example: true
        no__i_am_boss_ass_bitch:
          type: boolean
          description: "is this user admin"
          example: true
        


